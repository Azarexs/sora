package me.azarex.sora.databasing.mysql;

import com.zaxxer.hikari.HikariDataSource;
import me.azarex.sora.databasing.Credentials;
import me.azarex.sora.databasing.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;

public class MySQLDatabase implements Database {

    private Credentials credentials;
    private HikariDataSource hikari;

    private static final String POOL_NAME = "SoraPool";
    private static final String CLASS_NAME = "com.mysql.jdbc.jdbc2.optional.MysqlDataSource";

    public MySQLDatabase(Credentials credentials) {
        this.credentials = credentials;
    }

    @Override
    public void configure() {
        hikari = new HikariDataSource();
        hikari.setMaximumPoolSize(10);
        hikari.setPoolName(POOL_NAME);
        hikari.setDataSourceClassName(CLASS_NAME);
        hikari.addDataSourceProperty("serverName", credentials.getIPAddress());
        hikari.addDataSourceProperty("port", credentials.getPort());
        hikari.addDataSourceProperty("databaseName", credentials.getDatabaseName());
        hikari.addDataSourceProperty("user", credentials.getUsername());
        hikari.addDataSourceProperty("password", credentials.getPassword());
    }

    @Override
    public CompletableFuture<Void> execute(String statement) {
        return CompletableFuture.runAsync(() -> {
            Connection connection = null;
            PreparedStatement preparedStatement = null;

            try {
                connection = hikari.getConnection();
                preparedStatement = connection.prepareStatement(statement);
                preparedStatement.execute();
            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public CompletableFuture<Void> update(String statement) {
        return CompletableFuture.runAsync(() -> {
            Connection connection = null;
            PreparedStatement preparedStatement = null;

            try {
                connection = hikari.getConnection();
                preparedStatement = connection.prepareStatement(statement);
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public CompletableFuture<ResultSet> query(String statement) {
        return CompletableFuture.supplyAsync(() -> {
            Connection connection = null;
            PreparedStatement preparedStatement = null;

            try {
                connection = hikari.getConnection();
                preparedStatement = connection.prepareStatement(statement);

                return preparedStatement.executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        });
    }
}
