package me.azarex.sora.databasing;

public interface Credentials {
    String getIPAddress();
    String getPort();
    String getUsername();
    String getPassword();
    String getDatabaseName();

    static Credentials from(String address, String port, String databaseName,  String username, String password) {
        return new Credentials() {
            @Override
            public String getIPAddress() {
                return address;
            }

            @Override
            public String getPort() {
                return port;
            }

            @Override
            public String getDatabaseName() {
                return databaseName;
            }

            @Override
            public String getUsername() {
                return username;
            }

            @Override
            public String getPassword() {
                return password;
            }
        };
    }
}
