package me.azarex.sora.databasing.sqlite;

import com.zaxxer.hikari.HikariDataSource;
import me.azarex.sora.databasing.Database;
import org.bukkit.plugin.Plugin;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.CompletableFuture;

public class SQLiteDatabase implements Database {

    private HikariDataSource hikari;

    private static final String DRIVER = "org.sqlite.JDBC";
    private static final String POOL_NAME = "SoraPool";
    private static final String DATABASE_PATH = "jdbc:sqlite:plugins/PersonalInfo/database.db";

    public SQLiteDatabase(Plugin plugin) {
        hikari = new HikariDataSource();
        Path destination = Paths.get(plugin.getDataFolder().toPath() + File.separator + "/database.db");

        if (Files.notExists(destination)) {
            try {
                Files.createDirectory(destination.getParent());
                Files.createFile(destination);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void configure() {
        hikari.setPoolName(POOL_NAME);
        hikari.setDriverClassName(DRIVER);
        hikari.setJdbcUrl(DATABASE_PATH);
        hikari.setMaxLifetime(10000);
        hikari.setMaximumPoolSize(50);
    }

    @Override
    public CompletableFuture<Void> execute(String statement) {
        return CompletableFuture.runAsync(() -> {
            Connection connection = null;
            PreparedStatement preparedStatement = null;

            try {
                connection = hikari.getConnection();
                preparedStatement = connection.prepareStatement(statement);
                preparedStatement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public CompletableFuture<Void> update(String statement) {
        return CompletableFuture.runAsync(() -> {
            Connection connection = null;
            PreparedStatement preparedStatement = null;

            try {
                connection = hikari.getConnection();
                preparedStatement = connection.prepareStatement(statement);
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public CompletableFuture<ResultSet> query(String statement) {
        return CompletableFuture.supplyAsync(() -> {
            Connection connection = null;
            PreparedStatement preparedStatement = null;

            try {
                connection = hikari.getConnection();
                preparedStatement = connection.prepareStatement(statement);

                return preparedStatement.executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        });
    }
}
