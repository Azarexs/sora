package me.azarex.sora.databasing;

import java.sql.ResultSet;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface Database {

    void configure();
    CompletableFuture<Void> execute(String statement);
    CompletableFuture<Void> update(String statement);
    CompletableFuture<ResultSet> query(String statement);
}
